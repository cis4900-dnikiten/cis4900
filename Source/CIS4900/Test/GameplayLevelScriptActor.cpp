// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "ShooterGameInstance.h"
#include "GameplayLevelScriptActor.h"

void AGameplayLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	UShooterGameInstance * Instance = Cast<UShooterGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (Instance) Instance->OnLevelFinishedLoading(bShouldLoadOnBegin);
}


