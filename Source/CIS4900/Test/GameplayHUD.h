// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"

#include "GameplayHUD.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API AGameplayHUD : public AHUD
{
	GENERATED_BODY()
public:
	/*Called every frame*/
	UFUNCTION( BlueprintImplementableEvent, Category = Update) 
		virtual void UpdateStats(class UPlayerStats * Stats);

	/*Called when the player takes damage of any kind*/
	UFUNCTION(BlueprintImplementableEvent, Category = Event)
		virtual void OnTakeDamage(float ArmorDamage, float HealthDamage);

	/*Called when the player has 0 health*/
	UFUNCTION(BlueprintImplementableEvent, Category = Event)
		virtual void OnDead();

	/*Tremors the gui elements*/
	UFUNCTION(BlueprintImplementableEvent, Category = Event)
		virtual void Tremor();
};
