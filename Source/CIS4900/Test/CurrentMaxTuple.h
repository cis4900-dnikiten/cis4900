// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "Object.h"
#include "CurrentMaxTuple.generated.h"


USTRUCT()
struct FCurrentMaxStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite)
	int32 Current;
	UPROPERTY(BlueprintReadWrite)
	int32 Max;

	FCurrentMaxStruct() : Current(0), Max(0) {};
	FCurrentMaxStruct(int32 Cur, int32 M) : Current(Cur), Max(M) {};
};