// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Test/BaseWeapon.h"
#include "AssRifle.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API AAssRifle : public ABaseWeapon
{
	GENERATED_BODY()
public:

	AAssRifle();
	
protected:
	float DamagePerShot;

	UPROPERTY(EditDefaultsOnly, Category = Functionality)
	TSubclassOf<class ABullet> TracerRound;

	virtual void SuccesfulFire() override;
	
};
