// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "IUsable.generated.h"

/** Class needed to support InterfaceCast<IToStringInterface>(Object) */
UINTERFACE(MinimalAPI)
class UUsable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class IUsable
{
	GENERATED_IINTERFACE_BODY()

public:
	/*Will be called when the player wants to use the object*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Functionality|Use")
	void Use();

	/*Will be called when the player is looking at the object
	It should display the use text as a text render when called
	and return the displayed string
	*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Functionality|Use")
		FString ShowUseText();
	UFUNCTION(BlueprintImplementableEvent, Category = "Functionality|Use")
		void HideUseText();
};
