// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "IUsable.h"

//////////////////////////////////////////////////////////////////////////
// ToStringInterface

UUsable::UUsable(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

////This is required for compiling, would also let you know if somehow you called
////the base event/function rather than the over-rided version
//FString IUsable::UseText_Implementation()
//{
//	return "IUsable::UseText()";
//}
//
//void IUsable::Use_Implementation()
//{
//	
//}


