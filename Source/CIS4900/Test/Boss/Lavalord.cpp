// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "../GameplayCharacter.h"
#include "HammerProjectile.h"
#include "Lavalord.h"


// Sets default values
ALavalord::ALavalord() : WeaponSocketName("RightHand")
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SetState(ELavalordState::Type::LS_Idle);

	Hammer = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HammerMesh"));
	Hammer->AttachTo(GetMesh(), WeaponSocketName);

	DamageTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("DamageTrigger"));
	DamageTrigger->AttachTo(Hammer);

	WeakAttackDamage = 100.0f;

	PlayerLaunchForce = 1000.0f;
	PlayerLaunchAngle = 25.0f;

	bInCombat = false;
	bDidDamageThisSwing = false;
	bThrowingHammer = false;

	bDamageable = true;
	MaxHealth = 10000;
}

// Called when the game starts or when spawned
void ALavalord::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;
}

// Called every frame
void ALavalord::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if ( (bWeakAttack || bChargeAttack) && !bDidDamageThisSwing)
	{
		TriggerImpulse();
	}

}

FCurrentMaxStruct ALavalord::GetHealth()
{
	return FCurrentMaxStruct(CurrentHealth, MaxHealth);
}

void ALavalord::StartCharge()
{
	SetState(ELavalordState::Type::LS_Charging);

	GetCharacterMovement()->MaxWalkSpeed = ChargingSpeed;
}
void ALavalord::EndCharge()
{
	bChargeAttack = true;
	bDidDamageThisSwing = false;
	SetState(ELavalordState::Type::LS_Attacking);
	GetCharacterMovement()->MaxWalkSpeed = WalkingSpeed;
}

void ALavalord::WeakAttack()
{
	SetState(ELavalordState::Type::LS_Attacking);
	bWeakAttack = true;
}
void ALavalord::ThrowHammer()
{
	if (Enemy)
	{
		bThrowingHammer = true;
		SetState(ELavalordState::Type::LS_Attacking);
	}
}
void ALavalord::PickupHammer()
{
	SetState(ELavalordState::Type::LS_GrabHammer);
}


ELavalordState::Type ALavalord::GetCurrentState() { return CurrentState; }

FVector ALavalord::GetHammerThrowPoint()
{
	return FVector(0, 0, 0);
}

float ALavalord::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	if (bDamageable && CurrentState != ELavalordState::Type::LS_Dead)
	{
		CurrentHealth -= DamageAmount;
		if (CurrentHealth <= 0) Die();

		return DamageAmount;
	}

	return 0.0f;
}

void ALavalord::Die_Implementation()
{
	SetState(ELavalordState::Type::LS_Dead);
}

void ALavalord::SetState(ELavalordState::Type New)
{
	if (CurrentState != ELavalordState::Type::LS_Dead) CurrentState = New;
}

/*Blueprint callbacks*/

void ALavalord::EndWeakAttackCallback()
{
	SetState(ELavalordState::Type::LS_Idle);
	bWeakAttack = false;
	bDidDamageThisSwing = false;
}
void ALavalord::WeakAttackApexCallback()
{
	//TriggerImpulse();
}

void ALavalord::ThrowHammerApexCallback()
{
	if (!Enemy)
	{
		return;
	}

	if (Hammer) Hammer->SetVisibility(false);

	FVector Point = GetMesh()->GetSocketLocation(WeaponSocketName);

	FActorSpawnParameters Params;
	Params.Owner = this;
	Params.bNoCollisionFail = true;
	Params.bNoFail = true;

	SpawnedHammer = GetWorld()->SpawnActor<AHammerProjectile>(*HammerToSpawn, Point, GetActorRotation(), Params);
	if(SpawnedHammer) SpawnedHammer->FlyTo(Enemy->GetMesh());
}

void ALavalord::ThrowHammerEndCallback()
{
	SetState(ELavalordState::Type::LS_Idle);
	bThrowingHammer = false;

	if (Hammer) Hammer->SetVisibility(true);
}

void ALavalord::EndPickupCallback()
{
	SetState(ELavalordState::Type::LS_Idle);
}

void ALavalord::WeakAttackStopDamageCallback()
{
	bDidDamageThisSwing = true;
}

void ALavalord::EndChargeAttackCallback()
{
	SetState(ELavalordState::Type::LS_Idle);
	bChargeAttack = false;
}


void ALavalord::TriggerImpulse()
{
	TArray<AActor*> Actors;
	DamageTrigger->GetOverlappingActors(Actors, AGameplayCharacter::StaticClass());

	for (auto i : Actors)
	{
		AGameplayCharacter * Player = Cast<AGameplayCharacter>(i);

		if (Player && Player->bCanBeDamaged)
		{
			Player->TakeDamage(WeakAttackDamage, FDamageEvent(), GetController(), this);

			FVector Direction = (Player->GetActorLocation() - GetActorLocation());
			Direction = FVector(Direction.X, Direction.Y, 0.0f);

			FRotator Rot = FRotationMatrix::MakeFromX(Direction).Rotator();
			Rot = FRotator(FMath::Clamp(PlayerLaunchAngle, 0.0f, 90.0f), Rot.Yaw, Rot.Roll);
			FVector LaunchDir = Rot.Vector();
			LaunchDir.Normalize();
			Player->LaunchCharacter(LaunchDir*PlayerLaunchForce, true, true);

			bDidDamageThisSwing = true;
		}
	}
}