// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "SpawnHandler.h"


// Sets default values for this component's properties
ASpawnHandler::ASpawnHandler()
{

}


// Called when the game starts
void ASpawnHandler::BeginPlay()
{
	Super::BeginPlay();
}


/*If this spawner wants to spawn more actors*/
bool ASpawnHandler::HasMoreToSpawn_Implementation()
{
	return false;
}
/*This will spawn the actor and return a reference to it*/
AActor* ASpawnHandler::NextActor_Implementation()
{
	return nullptr;
}
/*This will be called when the spawning process wants to be started, state should be cleared here*/
void ASpawnHandler::StartSpawningProcess_Implementation()
{

}
/*Called if the spawning process needs to be prematurely stopped before all desired actors have spawned*/
void ASpawnHandler::StopSpawningProcess_Implementation()
{

}
