// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "ShooterGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API UShooterGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	static const int32 NumberOfWeapons = 4;
	UShooterGameInstance();
	
	const TArray<TSubclassOf<class ABaseWeapon>> GetAllPossibleWeaponClasses();
	
	UFUNCTION(BlueprintCallable, Category = Functionality)
		/*The level to change to, and, if Save = true the level to set as the current level for the slot
		NOTE: you can leave SlotCurrentLevel as none to use NewLevel, however this isnt always desirable*/
		void ChangeLevel(FName NewLevel, bool Save = false, FName SlotCurrentLevel = NAME_None);

	UFUNCTION(BlueprintImplementableEvent, Category = Functionality)
		void LoadingScreen();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void OnLevelFinishedLoading(bool Load = true);

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void  StartGame(FString SlotName, int32 UserIndex);

	UFUNCTION(BlueprintCallable, Category = Functionality)
		/*Will try to load the specified slot/index and return the level the same is on.
		If it cannot find it then it will return FName("")
		*/
		FName GetSlotCurrentLevel(FString SlotName, int32 UserIndex);

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void DeleteSave(FString Slot, int32 Index);

	UPROPERTY(BlueprintReadOnly, Category = Functionality)
		FName LoadingScreenLevelToLoad;

	

protected:

	UPROPERTY(BlueprintReadWrite, Category = Functionality)
	TArray<TSubclassOf<class ABaseWeapon>> AllWeaponClasses;

	FString Slot;
	uint32 Index;

	class AGameplayCharacter* FindPlayer();

	void SaveGame(FName Level = NAME_None);
	void LoadGame();

	/**CONSOLE DELEGATES**/
	UFUNCTION(Exec) void ConsoleForceSave();
	FConsoleCommandDelegate CFuncSave;
	UFUNCTION(Exec) void ConsoleForceLoad();
	FConsoleCommandDelegate CFuncLoad;
	UFUNCTION(Exec) void ConsoleDeleteSaves();
	FConsoleCommandDelegate CFuncDeleteSaves;
	//END DELEGATES
private:

	void AddBPClassToWeapons(const wchar_t* BPPath);
};
