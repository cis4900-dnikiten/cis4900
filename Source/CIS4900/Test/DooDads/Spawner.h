// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class CIS4900_API ASpawner : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* Root;

	UPROPERTY(VisibleDefaultsOnly, Category= Mesh)
	class USphereComponent* CollisionMesh;

public:	
	// Sets default values for this actor's properties
	ASpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	inline AActor* GetSpawnedActor() { return SpawnedActor; }

	UFUNCTION()
	void Spawn();
protected:

	UPROPERTY(EditAnywhere, Category = Functionality)
	float SpawnTime;
	UPROPERTY(EditAnywhere, Category = Functionality)
	TSubclassOf<AActor> ToSpawn;
	UPROPERTY(EditAnywhere, Category = Functionality)
	bool bWaitFirstTime;

	
	
private:
	FTimerHandle Handle;

	class AActor* SpawnedActor;
};
