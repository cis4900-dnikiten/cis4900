// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Test/Bullet.h"
#include "RailGunBullet.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API ARailGunBullet : public ABullet
{
	GENERATED_BODY()

public:
	ARailGunBullet();

	virtual void BeginPlay() override;

	void Init(int32 Damage, float PercentCharge);


	UFUNCTION()
	void BeginOverlap(AActor* Other);

protected:

	int32 Damage;
	bool bCanPierce;

};
