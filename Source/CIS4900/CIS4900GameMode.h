// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "CIS4900GameMode.generated.h"

UCLASS(minimalapi)
class ACIS4900GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ACIS4900GameMode();
};



