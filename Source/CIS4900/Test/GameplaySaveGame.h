// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "GameplaySaveGame.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API UGameplaySaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UGameplaySaveGame();
	UPROPERTY(VisibleAnywhere, Category = Character)
	float CurrentHP;
	UPROPERTY(VisibleAnywhere, Category = Character)
	float CurrentArmor;

	UPROPERTY(VisibleAnywhere, Category = Character)
	bool bHasWeapon2;
	UPROPERTY(VisibleAnywhere, Category = Character)
	bool bHasWeapon3;
	UPROPERTY(VisibleAnywhere, Category = Character)
	bool bHasWeapon4;
	
	UPROPERTY(VisibleAnywhere, Category = Character)
	int32 WeaponAmmo2;
	UPROPERTY(VisibleAnywhere, Category = Character)
	int32 WeaponAmmo3;
	UPROPERTY(VisibleAnywhere, Category = Character)
	int32 WeaponAmmo4;

	UPROPERTY(VisibleAnywhere, Category = Character)
	FName CurrentLevel;

};
