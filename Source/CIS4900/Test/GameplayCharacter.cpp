// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GameplayCharacter.h"
#include "GameplayPlayerController.h"
#include "Interfaces/IUsable.h"
#include "PlayerStats.h"

/*Used to initialy create a save game
Fills the save with the default values to initialize the player with
*/
void AGameplayCharacter::SaveDefault(UGameplaySaveGame* Save)
{
	Save->CurrentHP = PS_CURRENT_HEALTH_START;
	Save->CurrentArmor = PS_CURRENT_ARMOR_START;

	Save->bHasWeapon2 = false;
	Save->bHasWeapon3 = false;
	Save->bHasWeapon4 = false;

	Save->WeaponAmmo2 = 0;
	Save->WeaponAmmo3 = 0;
	Save->WeaponAmmo4 = 0;
}

// Sets default values
AGameplayCharacter::AGameplayCharacter() : MaxWalkSpeed(800), MaxSprintSpeed(3000), UseDistance(100)
{
	GetCapsuleComponent()->InitCapsuleSize(70.f, 96.0f);
	//Ignore trace channel 1, which should be PlayerWeapon
	GetCapsuleComponent()->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);

	GetMesh()->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);

	bUseControllerRotationYaw = false;

	FPCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FPCamera"));
	FPCameraComponent->AttachTo(GetCapsuleComponent());
	FPCameraComponent->RelativeLocation = FVector(0, 0, 64.f); // Position the camera
	FPCameraComponent->bUsePawnControlRotation = true;

	FPMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FPMesh"));
	FPMesh->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);
	FPMesh->AttachTo(FPCameraComponent);
	FPMesh->bOnlyOwnerSee = true;
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("Flashlight"));
	FLight->bAutoActivate = false;
	FLight->SetVisibility(false);
	FLight->AttachTo(FPCameraComponent);
	

	ShotStart = CreateDefaultSubobject<USceneComponent>(TEXT("ShotStart"));
	ShotStart->AttachTo(GetCapsuleComponent());

	StartingWeapons.Add(1);

	FlashlightState = EFlashlightEnum::Type::FE_OFF;
	FlashlightTickTime = 1.0f;

	bNoDamage = false;

	GetCharacterMovement()->MaxWalkSpeed = 800.0f;
}

void AGameplayCharacter::Load(UGameplaySaveGame* Save)
{
	GetStats()->Load(Save);
}
void AGameplayCharacter::Save(UGameplaySaveGame* Save)
{
	Save->CurrentHP = GetStats()->GetHealth().Current;
	Save->CurrentArmor = GetStats()->GetArmor().Current;

	Save->bHasWeapon2 = GetStats()->HasWeaponBySlot(2);
	Save->bHasWeapon3 = GetStats()->HasWeaponBySlot(3);
	Save->bHasWeapon4 = GetStats()->HasWeaponBySlot(4);

	Save->WeaponAmmo2 = GetStats()->GetGunInventoryAmmoBySlot(2).Current;
	Save->WeaponAmmo3 = GetStats()->GetGunInventoryAmmoBySlot(3).Current;
	Save->WeaponAmmo4 = GetStats()->GetGunInventoryAmmoBySlot(4).Current;
}

UPlayerStats* AGameplayCharacter::GetStats()
{
	return Stats;
}

void AGameplayCharacter::Flashlight()
{
	if (FLight)
	{
		if (FlashlightState == EFlashlightEnum::Type::FE_ON) FLight->ToggleVisibility();
		if (FlashlightState == EFlashlightEnum::Type::FE_OFF && GetStats()->GetBatteryPower().Current > 0) FLight->ToggleVisibility();

		if (FLight->IsVisible()) FlashlightState = EFlashlightEnum::Type::FE_ON;
		else if (!FLight->IsVisible()) FlashlightState = EFlashlightEnum::Type::FE_OFF;

		if (FlashlightToggleSound) UGameplayStatics::PlaySoundAttached(FlashlightToggleSound, GetRootComponent());
	}
}

float AGameplayCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	if (bNoDamage) return 0.0f;

	UPlayerStats* OurStats = GetStats();
	if (OurStats)
	{
		float ArmorDamage = FMath::RoundToInt(DamageAmount * ARMOR_PERCENT);
		float HealthDamage = FMath::RoundToInt(DamageAmount*(1.0f - ARMOR_PERCENT));

		float TmpCurrentArmor = OurStats->GetArmor().Current;
		float NewCurrentArmor = FMath::Max(TmpCurrentArmor - ArmorDamage, 0.0f);
		
		float OverflowArmorDamage = 0.0f;
		if (NewCurrentArmor == 0) OverflowArmorDamage = FMath::Abs(TmpCurrentArmor - ArmorDamage);

		float ArmorDamageDealt = FMath::Abs(TmpCurrentArmor - NewCurrentArmor);
		float HealthDamageDealt = OverflowArmorDamage + HealthDamage;
		
		OurStats->AddArmor((int32)(-ArmorDamage));
		OurStats->AddHealth(-(HealthDamageDealt));


		if (GetController())
		{
			AGameplayPlayerController* OurController = Cast<AGameplayPlayerController>(GetController());
			if (OurController)
			{
				OurController->TellHUDDamage(HealthDamageDealt, ArmorDamageDealt);
			}
		}

		return DamageAmount;
	}

	return 0.0f;
}

// Called when the game starts or when spawned
void AGameplayCharacter::BeginPlay()
{
	Super::BeginPlay();

	Stats = NewObject<UPlayerStats>();
	GetStats()->InitGunArray(GetWorld(),this, FPMesh, nullptr);
	AddStartingWeapons();

	EquipWeapon(1);

	GetWorldTimerManager().SetTimer(FlashlightHandle, this, &AGameplayCharacter::FlashlightTick, FlashlightTickTime, true);

	//Setting up delegates
	CFuncAddHealth.BindUObject(this, &AGameplayCharacter::ConsoleAddHealth);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("AddHealth"), TEXT("AddHealth <number>") TEXT("Will add <number> to current health"), CFuncAddHealth, 0);
	CFuncAddArmor.BindUObject(this, &AGameplayCharacter::ConsoleAddArmor);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("AddArmor"), TEXT("AddArmor <number>") TEXT("Will add <number> to current hrmor"), CFuncAddArmor, 0);
	CFuncAddAmmo.BindUObject(this, &AGameplayCharacter::ConsoleAddAmmo);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("AddAmmo"), TEXT("AddAmmo <number>") TEXT("Will add <number> to current weapons ammo"), CFuncAddAmmo, 0);
	CFuncGiveGun.BindUObject(this, &AGameplayCharacter::ConsoleGiveGun);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("GiveGun"), TEXT("GiveGun <Slot>") TEXT("Gives the gun that corresponds to <Slot>"), CFuncGiveGun, 0);
	CFuncInfiniteAmmoInventory.BindUObject(this, &AGameplayCharacter::ConsoleInfiniteAmmo);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("ToggleInfiniteAmmo"), TEXT("ToggleInfiniteAmmo") TEXT("Toggles infinite ammo for the current gun"), CFuncInfiniteAmmoInventory, 0);
	CFuncInfiniteHealth.BindUObject(this, &AGameplayCharacter::ConsoleInfiniteHealth);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("ToggleNoDamage"), TEXT("ToggleNoDamage") TEXT("Toggles if the character should take damage"), CFuncInfiniteHealth, 0);
}



// Called every frame
void AGameplayCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (GetStats() && GetStats()->GetHealth().Current == 0)
	{
		Die();
	}

	FHitResult Hit;
	FVector StartLoc, EndLoc;
	FRotator StartRot;
	GetActorEyesViewPoint(StartLoc, StartRot);

	EndLoc = StartLoc + StartRot.Vector() * UseDistance;
	
	if (GetWorld()->LineTraceSingleByChannel(Hit, StartLoc, EndLoc, USE_TRACE_CHANNEL))
	{
		if (Usable(ToUse)) IUsable::Execute_HideUseText(ToUse);

		ToUse = Hit.GetActor();

		if (Usable(ToUse)) IUsable::Execute_ShowUseText(ToUse);
	}
	else if (!Hit.bBlockingHit && Usable(ToUse))
	{
		IUsable::Execute_HideUseText(ToUse);
		ToUse = nullptr;
	}

	CheckCVars();
}

void AGameplayCharacter::FlashlightTick()
{
	UPlayerStats * OurStats = GetStats();

	if (FlashlightState == EFlashlightEnum::Type::FE_ON)
	{
		if (OurStats && OurStats->SubBatteryPower(BatteryDrainPerTick)) Flashlight();
	}
	else if (FlashlightState == EFlashlightEnum::Type::FE_OFF)
	{
		if (OurStats) OurStats->AddBatteryPower(BatteryChargePerTick);
	}
}

ABaseWeapon* AGameplayCharacter::GetCurrentWeapon()
{
	if (!GetStats()) return nullptr;
	return GetStats()->GetCurrentWeapon();
}

void AGameplayCharacter::GetShotStartPoint(FVector& Loc, FRotator& Rot)
{
	GetController()->GetPlayerViewPoint(Loc, Rot);
	Loc = ShotStart->GetComponentLocation();
}

void AGameplayCharacter::EquipWeapon(uint8 Slot)
{
	if (!GetStats()) return;

	ABaseWeapon* Current = GetStats()->GetCurrentWeapon();
	ABaseWeapon* ToEquip = GetStats()->MakeSlotCurrent(Slot);

	if (ToEquip)
	{
		if (Current) 
		{
			Current->StopFire();
			Current->SetActorHiddenInGame(true);
		}
		ToEquip->SetActorHiddenInGame(false);
	}
}

void AGameplayCharacter::Die()
{
	AGameplayPlayerController * OurController;

	//todo, die
	if (GetController())
	{
		OurController = Cast<AGameplayPlayerController>(GetController());

		if(OurController) OurController->TellHUDDead();
	}

}

void AGameplayCharacter::CheckCVars()
{
	/*int32 ConsoleHealth = CVarHealth.GetValueOnGameThread();
	if (ConsoleHealth != GetStats()->GetHealth().Current)
	{
		GetStats()->SetHealth(ConsoleHealth);
	}*/
}

// Called to bind functionality to input
void AGameplayCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis("MoveForward", this, &AGameplayCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AGameplayCharacter::MoveRight);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	InputComponent->BindAction("Sprint", IE_Pressed, this, &AGameplayCharacter::Sprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &AGameplayCharacter::StopSprinting);

	InputComponent->BindAction("Use", IE_Pressed, this, &AGameplayCharacter::Use);

	InputComponent->BindAction("WeaponSlot1", IE_Pressed, this, &AGameplayCharacter::Slot1);
	InputComponent->BindAction("WeaponSlot2", IE_Pressed, this, &AGameplayCharacter::Slot2);
	InputComponent->BindAction("WeaponSlot3", IE_Pressed, this, &AGameplayCharacter::Slot3);
	InputComponent->BindAction("WeaponSlot4", IE_Pressed, this, &AGameplayCharacter::Slot4);

	InputComponent->BindAction("Fire", IE_Pressed, this, &AGameplayCharacter::StartFire);
	InputComponent->BindAction("Fire", IE_Released, this, &AGameplayCharacter::StopFire);
	InputComponent->BindAction("Reload", IE_Pressed, this, &AGameplayCharacter::Reload);
	InputComponent->BindAction("ChangeWeaponUp", IE_Pressed, this, &AGameplayCharacter::ChangeWeaponSlotUp);
	InputComponent->BindAction("ChangeWeaponDown", IE_Pressed, this, &AGameplayCharacter::ChangeWeaponSlotDown);
	InputComponent->BindAction("FlashlightToggle", IE_Pressed, this, &AGameplayCharacter::Flashlight);
}

void AGameplayCharacter::Sprint()
{
	GetCharacterMovement()->MaxWalkSpeed = MaxSprintSpeed;
}
void AGameplayCharacter::StopSprinting()
{
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
}
void AGameplayCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AGameplayCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}
void AGameplayCharacter::ChangeWeaponSlot(float Direction)
{
	if (GetStats() && GetStats()->GetCurrentWeapon())
	{
		EquipWeapon(GetStats()->GetNextSlot(GetStats()->GetCurrentWeapon()->GetSlotNumber(), Direction));
	}
}

void AGameplayCharacter::ChangeWeaponSlotDown() { ChangeWeaponSlot(-1.0f); }
void AGameplayCharacter::ChangeWeaponSlotUp() { ChangeWeaponSlot(1.0f); }

void AGameplayCharacter::Use()
{
	if (Usable(ToUse)) IUsable::Execute_Use(ToUse);
}

void AGameplayCharacter::StartFire()
{
	if (bInCinematic) return;

	auto Cur = GetStats()->GetCurrentWeapon();
	if (Cur) Cur->StartFire();
}
void AGameplayCharacter::StopFire()
{
	if (bInCinematic) return;

	auto Cur = GetStats()->GetCurrentWeapon();
	if (Cur) Cur->StopFire();
}
void AGameplayCharacter::Reload()
{
	if (bInCinematic) return;

	auto Cur = GetStats()->GetCurrentWeapon();
	if (Cur) Cur->Reload();
}

void AGameplayCharacter::GiveWeaponByEnum(EGunEnum::Type T)
{
	UPlayerStats* Stats = GetStats();

	if (Stats) Stats->GiveWeaponByEnum(T);
	EquipWeapon(Stats->GetSlot(T));
}

//PRIVATE
void AGameplayCharacter::AddStartingWeapons()
{
	for (auto i : StartingWeapons)
	{
		if(GetStats()) GetStats()->GiveWeaponBySlot(i);
	}
}

bool AGameplayCharacter::Usable(UObject * ToCheck)
{
	return false;


	return (ToCheck && IsValid(ToCheck) && ToCheck->GetClass()->ImplementsInterface(UUsable::StaticClass()));

}

//END PRIVATE

/**Console Delegate handlers**/
void AGameplayCharacter::ConsoleAddHealth(const TArray<FString> & Params)
{
	if (Params.IsValidIndex(0))
	{
		int32 HealthToAdd = FCString::Atoi(Params[0].GetCharArray().GetData());
		GetStats()->AddHealth(HealthToAdd);
	}
}

void AGameplayCharacter::ConsoleAddArmor(const TArray<FString> & Params)
{
	if (Params.IsValidIndex(0))
	{
		int32 ArmorToAdd = FCString::Atoi(Params[0].GetCharArray().GetData());
		GetStats()->AddArmor(ArmorToAdd);
	}
}

void AGameplayCharacter::ConsoleAddAmmo(const TArray<FString> & Params)
{
	if (Params.IsValidIndex(0))
	{
		int32 AmmoToAdd = FCString::Atoi(Params[0].GetCharArray().GetData());
		GetStats()->AddAmmoCurrent(AmmoToAdd);
	}
}
void AGameplayCharacter::ConsoleGiveGun(const TArray<FString> & Params)
{
	if (Params.IsValidIndex(0))
	{
		int32 NewGunSlot = FCString::Atoi(Params[0].GetCharArray().GetData());
		GetStats()->GiveWeaponBySlot(NewGunSlot);
	}
}

void AGameplayCharacter::JustFired(EGunEnum::Type Gun)
{
	if (FPMesh && FireAnimation && FPMesh->GetAnimInstance())
	{
		FPMesh->GetAnimInstance()->Montage_Play(FireAnimation);
	}
}

void AGameplayCharacter::ConsoleInfiniteAmmo(const TArray<FString>& Params)
{
	if (GetStats() && GetStats()->GetCurrentWeapon())
	{
		bool NewState = !GetStats()->GetCurrentWeapon()->GetInfiniteInventoryAmmo();
		GetStats()->GetCurrentWeapon()->SetInfiniteInventoryAmmo(NewState);
	}
}
void AGameplayCharacter::ConsoleInfiniteHealth(const TArray<FString>& Params)
{
	bNoDamage = !bNoDamage;
}
/** End **/