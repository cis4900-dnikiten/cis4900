// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "HammerProjectile.generated.h"

UCLASS()
class CIS4900_API AHammerProjectile : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = Functionality)
		UProjectileMovementComponent* ProjComp;
	UPROPERTY(VisibleAnywhere, Category = Functionality)
		USceneComponent* Root;
	UPROPERTY(VisibleAnywhere, Category = Functionality)
		USkeletalMeshComponent* Mesh;

public:	
	// Sets default values for this actor's properties
	AHammerProjectile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void FlyTo(class USceneComponent* Point);
	
	UFUNCTION()
		void OnOverlap(AActor* Other);

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Functionality)
		float Damage;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Functionality)
		float LaunchForce;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Functionality)
		float LaunchAngle;
	
};
