// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "ConsoleManager.h"
#include "GameplayPlayerController.generated.h"

/**
 * 
 */
//setup a console variable for the sensitivity
static TAutoConsoleVariable<float> CVarSensitivity(TEXT("Sensitivity"), 1.0, TEXT("Set mouse sensitivity"));

UCLASS()
class CIS4900_API AGameplayPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AGameplayPlayerController();

	UFUNCTION() float GetSensitvity();
	UFUNCTION() void SetSensitivity(float nSen);

	void TellHUDDamage(float HealthDamage, float ArmorDamage);
	void TellHUDDead();

protected:
	virtual void PlayerTick(float delta) override;
	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;

	UFUNCTION()
	void MouseYAxis(float AV);
	UFUNCTION()
	void MouseXAxis(float AV);

private:
	//Sensitivity
	UPROPERTY() float MouseSensitivityMin = 0.1f;
	UPROPERTY() float MouseSensitivityMax = 2.0f;
	UPROPERTY() float MouseSensitivityCurrent = 1.0f;
	//
	UFUNCTION() void UpdateHUD();

	class AGameplayCharacter* Character;
	class AGameplayHUD* CurrentHUD;
};
