// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

UCLASS()
class CIS4900_API ABullet : public AActor
{
	GENERATED_BODY()

public:	

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USphereComponent* CollisionMesh;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		UParticleSystemComponent * TracerRibbon;
	UPROPERTY(VisibleDefaultsOnly, Category = Functionality)
		UProjectileMovementComponent* ProjectileMovement;

	// Sets default values for this actor's properties
	ABullet();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION()
	void OnCollision(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	/*Sets the time it can live depending on the maximum distance it can travel and its velocity*/
	void SetProperLifeSpan(int32 MaxDistance);
};
