// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GridTile.h"
#include "Grid.generated.h"

UCLASS()
class CIS4900_API AGrid : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* GRoot;

public:	
	// Sets default values for this actor's properties
	AGrid();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void OnConstruction(const FTransform & Trans) override;

	UFUNCTION(BlueprintImplementableEvent, Category = Functionality)
		void LevelShake(FVector PushDownPoint, float Force = 100.0f);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Functionality)
		UGridTile* GetClosestTileTo(const FVector & Point);
protected:
	/*the size of the grid : NxN*/
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Functionality)
		uint8 N;
	UPROPERTY(EditAnywhere, Category = Functionality)
		TSubclassOf<UGridTile> TileToSpawn;

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void ShakePlayerHUD();
	UFUNCTION(BlueprintNativeEvent, Category = Functionality)
		class AGameplayHUD* GetPlayerHUD();

	void BuildGrid();

	bool bBuilt;

private:

	uint8 OldN;

	float StartX, StartY;
};
