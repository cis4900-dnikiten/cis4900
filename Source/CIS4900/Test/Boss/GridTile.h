// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GridTile.generated.h"

UCLASS()
class CIS4900_API UGridTile : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UGridTile();

	UFUNCTION(BlueprintCallable, Category = Functionality)
	float GetTileSize();
	
protected:
	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		float TileSize;

};
