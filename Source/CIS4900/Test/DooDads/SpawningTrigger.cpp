// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "SpawningTrigger.h"

ASpawningTrigger::ASpawningTrigger() : BurstMethod(EBurstEnum::Type::BE_Linear), HandlersPerBurst(1), Bursts(0), TimeBetweenBursts(1.0f)
{
	CurrentHandlerIndex = 0;
	HandlersHandled = 0;
	CurrentBurst = 0;
	bHasBeenTriggered = false;

	bStartActivated = false;

	bOnlyTriggerOnce = false;

	GetCollisionComponent()->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);
	GetCollisionComponent()->SetCollisionResponseToChannel(BADMAN_TRACE_CHANNEL, ECR_Ignore);
	GetCollisionComponent()->SetCollisionResponseToChannel(USE_TRACE_CHANNEL, ECR_Ignore);
}
void ASpawningTrigger::BeginPlay()
{
	//Instance all our handlers
	for (auto i : BurstList)
	{
		auto NewSpawnHandler = GetWorld()->SpawnActor<ASpawnHandler>(*i);
		NewSpawnHandler->AttachRootComponentToActor(this);
		if(NewSpawnHandler) InstancedHandlers.Add(NewSpawnHandler);
	}

	if (bStartActivated) Trigger();
}

#if WITH_EDITOR
void ATriggerBox::EditorApplyScale(const FVector& DeltaScale, const FVector* PivotLocation, bool bAltDown, bool bShiftDown, bool bCtrlDown)
{
	Super::EditorApplyScale(DeltaScale, PivotLocation, bAltDown, bShiftDown, bCtrlDown);
	//SetActorScale3D(GetActorScale3D() + DeltaScale);
}
#endif

void ASpawningTrigger::Tick(float Delta)
{

}

void ASpawningTrigger::Trigger(bool bStart)
{
	if (bOnlyTriggerOnce && bHasBeenTriggered) return;

	if (bStart && !GetWorldTimerManager().IsTimerActive(BurstHandle))
	{
		StartProcess();
	}
	else if (!bStart)
	{
		StopProcess();
	}
}
void ASpawningTrigger::StartProcess()
{
	float FirstDelay = bWaitFirstBurst ? -1.0f : 0.0f;
	GetWorldTimerManager().SetTimer(BurstHandle, this, &ASpawningTrigger::Burst, TimeBetweenBursts, true, FirstDelay);
}
void ASpawningTrigger::StopProcess()
{
	CurrentHandlerIndex = 0;
	HandlersHandled = 0;
	CurrentBurst = 0;

	GetWorldTimerManager().ClearTimer(BurstHandle);

	bHasBeenTriggered = true;
}

bool ASpawningTrigger::IsActive()
{
	return GetWorldTimerManager().IsTimerActive(BurstHandle);
}

void ASpawningTrigger::Burst()
{
	//if we want to burst an infinite amout of times or we havent burst enough yet
	if (ShouldBurst())
	{
		CurrentHandlerIndex = 0;
		//Then process HandlersPerBurst amount of handlers
		while (ContinueBurst())
		{
			HandleSpawnHandler(GetNextSpawnHandler());
		}

		HandlersHandled = 0;
		CurrentBurst++;
	}
	else
	{
		StopProcess();
	}
}

//PRIVATE
ASpawnHandler* ASpawningTrigger::GetNextSpawnHandler()
{
	if (InstancedHandlers.Num() == 0) return nullptr;
	if (BurstMethod == EBurstEnum::Type::BE_Linear)
	{
		auto Ret = InstancedHandlers[CurrentHandlerIndex];
		
		CurrentHandlerIndex++;
		CurrentHandlerIndex = CurrentHandlerIndex >= InstancedHandlers.Num() ? 0 : CurrentHandlerIndex;
		
		return Ret;
	}
	else if (BurstMethod == EBurstEnum::Type::BE_Random)
	{
		return InstancedHandlers[FMath::RandRange(0, InstancedHandlers.Num()-1)];
	}

	return InstancedHandlers[0];
}
void ASpawningTrigger::HandleSpawnHandler(ASpawnHandler* ToHandle)
{
	HandlersHandled++;

	if (!ToHandle) return;

	ToHandle->StartSpawningProcess();

	while (ToHandle->HasMoreToSpawn())
	{
		auto NewActor = ToHandle->NextActor();
		if(NewActor) NewActor->SetActorLocation(GetSpawnLocation());
	}

	ToHandle->StopSpawningProcess();
}

FVector ASpawningTrigger::GetSpawnLocation()
{
	return FMath::RandPointInBox(GetComponentsBoundingBox());
}

bool ASpawningTrigger::ContinueBurst()
{
	return  (HandlersPerBurst > HandlersHandled);
}
bool ASpawningTrigger::ShouldBurst()
{
	return (Bursts == 0 || Bursts > CurrentBurst);
}

