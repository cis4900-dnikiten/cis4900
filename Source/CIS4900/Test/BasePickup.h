// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BasePickup.generated.h"

UCLASS()
class CIS4900_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class USphereComponent * CollisionMesh;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class USkeletalMeshComponent* VisibleMeshSkeletal;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class UStaticMeshComponent* VisibleMeshStatic;

public:	

	UPROPERTY(EditDefaultsOnly, Category = Functionality)
	USoundBase* PickupSound;

	// Sets default values for this actor's properties
	ABasePickup();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION()
	void OnCollision(AActor* OtherActor);
	
	UFUNCTION(BlueprintCallable, Category = Sound)
	void PlayPickupSound();

	UFUNCTION(BlueprintImplementableEvent, Category = Collision)
	virtual void HitGameplayCharacter(class AGameplayCharacter* Hit);
};
