// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Test/BaseWeapon.h"
#include "RailGunBullet.h"
#include "RailGun.generated.h"

USTRUCT()
struct FRailGunShotParams
{
	GENERATED_USTRUCT_BODY()
public:

	bool bBounce;

	int32 Damage;

	bool bPierce;

	int32 TimesToBounce;

	int32 TimesToPierce;

	UParticleSystem* CurrentSystem;

	FRailGunShotParams() :bBounce(false), Damage(0), bPierce(false), TimesToBounce(5), TimesToPierce(5){}

	FRailGunShotParams(float PercentCharged, int32 BulletDamage, int32 Bounce, int32 Pierce, UParticleSystem* L1, UParticleSystem*L2, UParticleSystem* L3, UParticleSystem* L4 );
};

/**
 * 
 */
UCLASS()
class CIS4900_API ARailGun : public ABaseWeapon
{
	GENERATED_BODY()

public:
	ARailGun();
	

	/*First threshold to hit to gain its benefits, in percent*/
	static const uint8 Threshold1 = 25;
	/*Second threshold to hit to gain its benefits, in percent*/
	static const uint8 Threshold2 = 75;
	/*Third threshold to hit to gain its benefits, in percent*/
	static const uint8 Threshold3 = 100;

	virtual void StartFire() override;
	virtual void StopFire() override;
	virtual bool Reload() override;
	virtual void OnSwitched() override;

	virtual void BeginPlay() override;

	void ReleaseCharge();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		uint8 GetThreshold1();
	UFUNCTION(BlueprintCallable, Category = Functionality)
		uint8 GetThreshold2();
	UFUNCTION(BlueprintCallable, Category = Functionality)
		uint8 GetThreshold3();

protected:
	FTimerHandle ChargeHandle;

	/*Beam particle system to use for a under threshold1 shot*/
	UPROPERTY(EditDefaultsOnly, Category = FX)
		UParticleSystem* Level1BeamSystem;
	/*Beam particle system to use for a threshold1 shot*/
	UPROPERTY(EditDefaultsOnly, Category = FX)
		UParticleSystem* Level2BeamSystem;
	/*Beam particle system to use for a threshold2 shot*/
	UPROPERTY(EditDefaultsOnly, Category = FX)
		UParticleSystem* Level3BeamSystem;
	/*Beam particle system to use for a threshold3 shot*/
	UPROPERTY(EditDefaultsOnly, Category = FX)
		UParticleSystem* Level4BeamSystem;

	UPROPERTY(EditDefaultsOnly, Category = Animation) UAnimMontage* ChargeMontage;

	/*Time to wait inbetween each charge ie if 0.1 then every 0.1 seconds the gun will gain ChargeAmount charge if the user wants to fire*/
	UPROPERTY(EditDefaultsOnly, Category = "Functionality|Shooting", meta=(ClampMin="0.01"))
	float ChargeDelay;
	/*Amount to add to charge every ChargeDelay seconds when user wants to fire*/
	UPROPERTY(EditDefaultsOnly, Category = "Functionality|Shooting")
	int32 ChargeAmount;

	/*The maximum number of bounces a beam can do if charged enough*/
	UPROPERTY(EditDefaultsOnly, Category = "Functionality|Shooting")
		int32 MaxBounces;
	/*The maximum number of enemies a single beam can pierce if charged enough*/
	UPROPERTY(EditDefaultsOnly, Category = "Functionality|Shooting")
		int32 MaxPierce;

	UFUNCTION()
	void Charge();

	void MakeBeam(FVector Source, FVector Target, UParticleSystem* BeamSystem);

	bool CanPierce(AActor* Actor);

	TArray<FHitResult> PierceTrace(const FVector& StartLoc, const FRotator& StartRot, bool Pierce = true);

private:
	void CalculateBounce(FRotator& NewDirection,const FVector& VectorToBounce, const FHitResult& Hit);

	UAnimInstance* AnimInstance;

	void StartChargeAnimation();
	void EndChargeAnimation();
};
