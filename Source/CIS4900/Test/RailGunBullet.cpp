// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "RailGunBullet.h"


ARailGunBullet::ARailGunBullet()
{
	USphereComponent* Collision = Cast<USphereComponent>(GetRootComponent());

	if (Collision)
	{
		Collision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
		Collision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);	
	}

	ProjectileMovement->InitialSpeed = 3000.0f;
	ProjectileMovement->MaxSpeed = 3000.0f;


	FParticleSysParam Source, Target;
	Source.Name = FName("BeamSource");
	Source.ParamType = EParticleSysParamType::PSPT_Vector;
	TracerRibbon->InstanceParameters.Add(Source);

	Target.Name = FName("BeamTarget");
	Target.ParamType = EParticleSysParamType::PSPT_Vector;
	TracerRibbon->InstanceParameters.Add(Target);

	OnActorBeginOverlap.AddDynamic(this, &ARailGunBullet::BeginOverlap);
}

void ARailGunBullet::BeginOverlap(AActor* Other)
{
	//TracerRibbon->SetBeamTargetPoint(0, GetActorLocation(), 0);
	TracerRibbon->SetVectorParameter(FName("BeamTarget"), GetActorLocation());
	TracerRibbon->ActivateSystem();

	//TracerRibbon->SetBeamSourcePoint(0, GetActorLocation(), 0);
	TracerRibbon->SetVectorParameter(FName("BeamSource"), GetActorLocation());

	APawn* Damagable = Cast<APawn>(Other);

	if (Damagable)
	{
		Damagable->TakeDamage(Damage, FDamageEvent(), nullptr, nullptr);
	}
}

void ARailGunBullet::BeginPlay()
{
	SetLifeSpan(0.5f);

	//TracerRibbon->SetBeamSourcePoint(0, GetActorLocation(), 0);

	TracerRibbon->SetVectorParameter(FName("BeamSource"), GetActorLocation());
}

void ARailGunBullet::Init(int32 BulletDamage, float PercentCharged)
{

}

