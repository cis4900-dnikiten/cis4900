// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "Bullet.h"


// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionMesh = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionMesh"));
	
	CollisionMesh->SetCollisionObjectType(ECC_GameTraceChannel3);

	CollisionMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionMesh->SetCollisionResponseToAllChannels(ECR_Block);
	CollisionMesh->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);
	CollisionMesh->SetCollisionResponseToChannel(ECC_GameTraceChannel3, ECR_Ignore);
	SetRootComponent(CollisionMesh);

	TracerRibbon = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Tracer"));
	TracerRibbon->AttachTo(GetRootComponent());


	TracerRibbon->bAutoActivate = false;

	SetActorEnableCollision(true);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("PMovement"));
	ProjectileMovement->InitialSpeed = 3000.0f;
	ProjectileMovement->MaxSpeed = 3000.0f;
	ProjectileMovement->ProjectileGravityScale = 0.0f;

	OnActorHit.AddDynamic(this, &ABullet::OnCollision);
	//OnActorBeginOverlap.AddDynamic(this, &ABullet::OnCollision);
}

void ABullet::SetProperLifeSpan(int32 Distance)
{
	SetLifeSpan(Distance / ProjectileMovement->InitialSpeed);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();

	if (TracerRibbon)
	{
		TracerRibbon->ActivateSystem();
	}
}

// Called every frame
void ABullet::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABullet::OnCollision(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	AActor* OwningActor = GetWorld()->GetFirstPlayerController()->GetCharacter();
	if (OwningActor != OtherActor)
	{
		Destroy();
	}
}
